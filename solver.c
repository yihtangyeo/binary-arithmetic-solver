/*
University of Toronto Faculty of Applied Science and Engineering
Division of Engineering Science
CSC192 Computer Programming and Data Structures

Title: Assignment 4
Question 1: solver.c

Student name: YEO, Yih Tang
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct node *stack;         // define new data type stack as pointer to struct node
struct node
 {
     int lhs;                       // lhs stores the numerical value to the left of operator
     int rhs;                       // rhs stores the numerical value to the right of operator, if available
     char op;                       // op stores the mathematical operation, either + or - or / or *
     stack next;                    // pointer to the next "stack"
 };

stack buildEmptyStack(stack first);                             // function that creates a new stack when a open bracket is found, and return the pointer to the topmost stack
void modifyStack (stack first, char ch, int n1, int n2);        // function that modifies the topmost stack as the inputFile is being read
stack evaluateStack(stack first);                               // function that evaluates the topmost stack, pop out the value, delete the stack, and store the poped out value into the new topmost stack, and return the pointer to the new topmost stack
int performOperation(char op, int num1, int num2);              // function that takes in two values, and evaluate them based on the operation specified, and return the value

// convert char to int without using atoi(), as atoi might produce wrong conversion, ie. 3 is randomly converted into 30.
int performConversion(char ch){
    // using the difference with ASCII code number 48, we are able to find the integer, eg. 8 = (ASCII 56) - ASCII 48 = 8
    return ((int)ch-48);
}

int main(void)
{

    char currentChar;               // temporary variable used to store each character being read from inputFile

    stack first = NULL;             // set the pointer to the first (topmost) stack to NULL, since there is no stack at all

    // prompt user to enter the file name that contains the mathematical expression
    printf("Please enter the equation: ");



        // since this program will evaluate the final value if a bracket is detected at the end (aka an empty stack with an op of '?')
        // virtually create an empty bracket for the file being read, if let's say the whole operation is not enclosed by bracket
        // for example, input-file is 1+2. This line of code virtually makes the input-file to be (1+2) by creating an empty stack
        first = buildEmptyStack(first);

        /*
        declare three variables of type char, which stores the last two and the current type of character being read.
        They can be:
        - o which is open bracket
        - c which is close bracket
        - n which is numerical value
        - m which is mathematical constant
        - w which is whitespace character (only used when numbers involved)
        */
        char previousTwoType;
        char previousType;
        char currentType;

        // this variable keeps track of how many brackets are opened at one time, which at the end of reading the inputFile, it should be zero.
        // when an open bracket is detected, it is incremented, when a close bracket is detected, it will be decremented
        int numberOfBracketsOpened = 0;

        /*
        WHY STORE THREE HISTORY?
        Note that there are only four kinds of character that can be expected from the file: A NUMBER, A BRACKET, A WHITESPACE or A MATH OPERATOR.
        So to determine if they are in correct format (not invalid input), we can test the location of the number in relative to the bracket and the
        math operator; the location of bracket in relative to the number and math operator, and the location of math operator in relative to the
        number and bracket. How to test it will be documented in the code below. The "w" whitespsace is just to check if a space exists between two numbers,
        and therefore will only be used when numbers are involved.
        */

        do{


            // the algorithm used here is to store the last two types of characters, so the characters should be different. If the characters read from
            // previous input is the same as previous two input, then don't change the type.  let's say we are reading 123 as 1 and 2 and 3 using fgetc,
            // we definitely don't want previousTwoType and previousType and currentType are all 'n' (numerical)
            // what the code done here is to move each history back by one if they are different
            if (previousTwoType != previousType)
                previousTwoType = previousType;
            if (previousType != currentType)
                previousType = currentType;

            // read from standard input
            currentChar = fgetc(stdin);

            // ignore whitespace characters, skip through the rest of the code, go back to the do{} part at the begining of this scope
            if (currentChar == ' '){

                // to make sure that in between numbers there is no whitespace, record the whitespace as history after a number is detected
                // the history will be analyzed when a number is detected after a whitespace. Note that multiple whitespace between a number
                // will only be detected once, since recording history of same type is not possible, as stated in the IF statement
                if (previousType == 'n'){
                    currentType = 'w';
                }
                else{
                    continue;
                }
            }
            // if the character is an open bracket
            else if (currentChar == '('){
                // increment the number of open bracket
                numberOfBracketsOpened++;

                // create an empty stack
                first = buildEmptyStack(first);

                // this is an open bracket, store this in history
                currentType = 'o';

                // an open bracket cannot have a number in front of it, or a close bracket in front of it
                if (previousType == 'n' || previousType == 'c'){
                    printf("Error: invalid input 1\n");
                    exit(EXIT_FAILURE);
                }
            }
            // if the character is a close bracket
            else if (currentChar == ')'){
                // decrement the number of open bracket
                numberOfBracketsOpened--;

                // if the number of open bracket gets less than zero, something is wrong. Print error and exit.
                if (numberOfBracketsOpened < 0){
                    printf("Error: Invalid input 2\n");
                    exit(EXIT_FAILURE);
                }

                // evaluate the topmost stack and pop the value out and store it into the new topmost stack
                first = evaluateStack(first);

                // this is a close bracket, store into memory
                currentType = 'c';

                // a close bracket cannot have a math operator in front of it, or a open bracket. If so, print error and exit.
                if (previousType == 'm'|| previousType == 'o'){
                    printf("Error: invalid input 3\n");
                    exit(EXIT_FAILURE);
                }
            }
            // if the character is one of the math operators
            else if (currentChar == '+' || currentChar == '-' || currentChar == '/' || currentChar == '*'){
                // put this into history
                currentType = 'm';

                // the error detection is fairly complicated for math operator
                // - the previous type cannot be a operator itself
                // - the previous type cannot be a open bracket
                // - if let's say the previous type is a number (which is OK), but if before te number there is another math operator (which sums up to 3 numerical constant in a single bracket), it is wrong
                if (previousType == 'm' || previousType == 'o' || (previousType == 'n' && previousTwoType == 'm')){
                    printf("Error: invalid input 4\n");
                    exit(EXIT_FAILURE);
                }
                else{
                    // when trying to store the math operator into the current topmost stack, the operator should be ?, since there is no
                    // previous operator being stored. If the operator is NOT '?', that means the math operator is somehow duplicated twice,
                    // and it is an error. Print error and exit.
                    if (first->op != '?'){
                        printf("Error: invalid input 5\n");
                        exit(EXIT_FAILURE);
                    }
                    // if it is the first time adding operator to the topmost stack, it is OK, store it.
                    else{
                        first->op = currentChar;
                    }

                }
            }
            // if the current character being read has an ASCII code from 48 to 57, aka from 0 to 9
            else if (currentChar >= 48 && currentChar <= 57){
                // this is type number
                currentType = 'n';

                // in front of a number, there shouldn't be any close bracket. If there is, print error and exit.
                if (previousType == 'c'){
                    printf("Error: invalid input 6\n");
                    exit(EXIT_FAILURE);
                }
                // if the previous one was a whitespace and the previous previous is a number, this is an error, since whitespace is not allowed between numbers
                else if (previousTwoType == 'n' && previousType == 'w'){
                    printf("Error: invalid input 7\n");
                    exit(EXIT_FAILURE);
                }

                // if the previous character read is also a number OR when the lhs/rhs is zero (unintialized), that means the number is a continuition from the previous one
                if (previousType == 'n' || first->lhs == 0 || first->rhs == 0){


                    // check the operation, if it is not defined, that means the left is not defined. Because if the right is only defined after the operator is defined.
                    // this will help the program to decided whether to store on the left hand side or the right hand side
                    if (first->op == '?'){
                        // take the previous number in the lhs and move it up by another digit (from one to ten, or from ten to hundred, etc.)
                        first->lhs = first->lhs*10 + performConversion(currentChar);

                        // if this number is beyond the range of 1-50, throw an error and exit
                        if (first->lhs > 50 || first->lhs < 1){
                            printf("Error: invalid input\n");
                            exit(EXIT_FAILURE);
                        }
                    }
                    else{
                        // take the previous number in the lhs and move it up by another digit (from one to ten, or from ten to hundred, etc.)
                        first->rhs = first->rhs*10 + performConversion(currentChar);

                        // if this number is beyond the range of 1-50, throw an error and exit
                        if (first->rhs > 50 || first->rhs < 1){
                            printf("Error: invalid input\n");
                            exit(EXIT_FAILURE);
                        }
                    }
                }
            }
            // if the character read is an EOF character, that means the file reaches its end. Stop reading and jump out of the the loop
            else if (currentChar == '\n'){
                break;
            }
            // if the characters are not all these, those are invalid input, print error and exit.
            else{
                printf("Error: invalid input 10\n");
                exit(EXIT_FAILURE);
            }
            // Note: personal usage. Debugger.
            //printf("\nDEBUG = LHS: %d, RHS: %d, OP: %c, memory: %d\n", first->lhs, first->rhs, first->op, first);

        } while (currentChar != '\n');

        // after finish reading the file and the brackets are not balanced, print error and exit
        if (numberOfBracketsOpened != 0){
            printf("Error: invalid input 11\n");
            exit(EXIT_FAILURE);
        }

        // by this time the linked list of stacks should be ONLY ONE STACK LEFT.
        // this STACK may or may not contain a valid operator, but anyhow we still have to evaluate it

        int result;

        // if the operator is not defined, then the left value is the result because it has been evaluated by a higher stack and is returned as a single value
        if (first->op == '?'){
            result = first->lhs;
        }
        // if there is an operator, call the performOperation function to determine the final result
        else{
            result = performOperation(first->op, first->lhs, first->rhs);
        }

        // print out the result
        printf("\nResult of the operations: %d\n", result);


    return 0;
}


stack buildEmptyStack(stack first)
{
    stack new_node;                             // create a new pointer point to a struct
    new_node = malloc(sizeof(struct node));     // allocate dynamic memory
    if (new_node == NULL){                      // if the memory is not able to be allocated, print error and exit
        printf("Error: Insufficient memory to allocate for new stack\n");
        exit(EXIT_FAILURE);
    }

    // initialize all elements in the struct as 0, 0 and ? to show that all of them are uninitialized
    new_node->lhs = 0;
    new_node->op = '?';
    new_node->rhs = 0;

    // link the previous struct together with pointer
    new_node->next = first;

    // return new "first" as the new node becomes the topmost stack
    return new_node;
}

int performOperation(char op, int num1, int num2)
{
    int result = 0;         // variable to store result

    // perform operation depends on operator
    switch (op){
        case '+':
            result = num1 + num2;
            break;
        case '-':
            result = num1 - num2;
            break;
        case '*':
            result = num1 * num2;
            break;
        case '/':
            result = num1 / num2;
            break;
    }

    // return the result of the calculation
    return result;
}

stack evaluateStack(stack first)
{
    int result;                                     // variable that stores the value evaluated at the topmost stack and to be pop-out

    // in case there are too many nested brackets that basically do nothing, the result should be the left hand side value only.
    // consider ((((3)))) the lhs will be 3 and the op will be ? and the rhs will be 0
    if (first->op == '?' && first->rhs == 0){
        result = first->lhs;
    }
    else{
        // or else, if there ia a valid value for operator and a valid number in rhs, evaluate the result by calling the function
        result = performOperation(first->op, first->lhs, first->rhs);
    }

    // since the old stack will be removed, define a new variable that points to the new topmost value
    stack newFirst = first->next;
    free(first);        // deallocate the memory space since this old topmost stack is deleted.
    first = newFirst;       // pointer points to NULL to avoid being able to change the value of that particular address by accident

    // if the operator is not defined, that means lhs has NOT been filled up, therefore, store the evaluated value into the lhs
    if (newFirst->op == '?'){
        newFirst->lhs = result;
    }
    // if the operator is defined, that means lhs has been filled up, therefore, store the evaluated value into the rhs
    else{
        newFirst->rhs = result;
    }


    // return the pointer pointing to the topmost layer
    return first;
}
